package sn.hiolabs.kafkaexample;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class KafkaListeners {

    @KafkaListener(topics = "hiolabs", groupId = "hio", containerFactory = "factory")
    void listener(MessageRequest data) {
        System.out.println("Listener received " + data + " \uD83D\uDE07 ");
    }

}
