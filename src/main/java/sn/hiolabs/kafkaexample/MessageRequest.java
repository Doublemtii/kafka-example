package sn.hiolabs.kafkaexample;

import java.time.LocalDateTime;

public record MessageRequest(String message, LocalDateTime createdAt) {
}
