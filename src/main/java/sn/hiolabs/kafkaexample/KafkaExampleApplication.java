package sn.hiolabs.kafkaexample;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.core.KafkaTemplate;

import java.time.LocalDateTime;

@SpringBootApplication
public class KafkaExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(KafkaExampleApplication.class, args);
    }

    @Bean
    CommandLineRunner commandLineRunner(KafkaTemplate<String, MessageRequest> kafkaTemplate) {
        return args -> {
            for (int i = 0; i <= 1_000; i++) {
                kafkaTemplate.send("hiolabs",
                        new MessageRequest("Hello Kafka ! " + i, LocalDateTime.now()));
            }
        };
    }
}
